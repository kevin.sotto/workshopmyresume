document.getElementById("aboutme").addEventListener("click", uploadDataAboutme);
document.getElementById("contact").addEventListener("click", uploadDataContactmeAt);
document.getElementById("habilities").addEventListener("click", uploadDataHabilities);
document.getElementById("experience").addEventListener("click", uploadDataExperience);
document.getElementById("academic").addEventListener("click", uploadDataAcademicBackground);

let information = document.getElementById("information")

//Creamos la función que nos traerá los datos al dar clic sobre la ventana
function uploadDataAboutme() {
//then = entonces  
//Realizamos la petición de red a los archivos en el JSON y los retornamos  
  fetch("datos.json")
    .then(function (res) {
      return res.json();
    })
    .then(function (data) {
      let html = "";
      for (let key in data.aboutme) {
        let value = data.aboutme[key];
        html += `<li class ='informationList'>${value}</li>`;
      }
      information.innerHTML = html
    });
}
//Función que nos trae los dtos de contactame
function uploadDataContactmeAt() {
    fetch("datos.json")
      .then(function (res) {
        return res.json();
      })
      .then(function (data) {
        let html = "";
        for (let key in data.contactmeAt) {
          let value = data.contactmeAt[key];
          html += `<li class ='informationList'>${value}</li>`;
        }
        information.innerHTML = html
      });
  }
//Funcion que nos trae los datos de habilidades
  function uploadDataHabilities() {
    fetch("datos.json")
      .then(function (res) {
        return res.json();
      })
      .then(function (data) {
        let html = "";
        for (let key in data.habilities) {
          let value = data.habilities[key];
          html += `<li class ='informationList'>${value}</li>`;
        }
        information.innerHTML = html
      });
  }
//Función que nos trae los datos de experience
  function uploadDataExperience() {
    fetch("datos.json")
      .then(function (res) {
        return res.json();
      })
      .then(function (data) {
        let html = "";
        for (let key in data.experience) {
          let value = data.experience[key];
          html += `<li class ='informationList'>${value}</li>`;
        }
        information.innerHTML = html
      });
  }
//Función que nos trae los datos de formación academica
function uploadDataAcademicBackground() {
  fetch("datos.json")
    .then(function (res) {
      return res.json();
    })
    .then(function (data) {
      let html = "";
      for (let key in data.academicBackground) {
        let value = data.academicBackground[key];
        html += `<li class ='informationList' >${value}</li>`;
      }
      information.innerHTML = html
    });
}
